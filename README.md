# eZ-rM2mouse

Automates the connection process of [rmWacomToMouse](https://github.com/LinusCDE/rmWacomToMouse). Just plug in your reMarkable and run the .exe. \
Note that you have to
* do all the prerequisites mentioned in the setup of rmWacomToMouse
* install the python module [Paramiko](https://www.paramiko.org) (`python -m pip install paramiko`)
* change the password in ssh_connection.py to the one used by your reMarkable (go to Settings -> About and scroll down to find it)

**LinusCDE did all of the work, all the relevant files are from them.** \
This really just starts the python script on your Tablet and PC.

![](https://i.kym-cdn.com/entries/icons/original/000/028/021/work.jpg)
import paramiko


ssh = paramiko.client.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect("10.11.99.1", port=22,
            username="root", password="YOURPASSWORDHERE")  # enter your reMarkable SSH password here
# (if you don't know the password, go to Settings -> About and scroll down to the bottom)
ssh.exec_command("./rmServeWacomInput")
